out/main: obj/main.o obj/OffscreenRender.o obj/App.o obj/Controls.o obj/ShaderPipeline.o  obj/VertexShader.o obj/FragmentShader.o obj/Shader.o obj/MeshLoader.o obj/Model.o obj/Mesh.o
	g++ -o out/main obj/main.o obj/App.o obj/Controls.o obj/ShaderPipeline.o obj/OffscreenRender.o obj/VertexShader.o obj/FragmentShader.o obj/Shader.o obj/MeshLoader.o obj/Mesh.o obj/Model.o -lglut -framework opengl -Llibs/ -lassimp -lIrrXML -std=c++14 

obj/main.o: src/main.cpp
	g++ -c -Iinclude src/main.cpp -std=c++14 -o obj/main.o

obj/App.o: src/App.cpp
	g++ -Wno-deprecated -c -Iinclude src/App.cpp -std=c++14 -o obj/App.o 

obj/Controls.o: src/Controls.cpp
	g++ -Wno-deprecated -c -Iinclude src/Controls.cpp -std=c++14 -o obj/Controls.o

obj/MeshLoader.o: src/MeshLoader.cpp
	g++ -c -Iinclude src/MeshLoader.cpp -std=c++14 -o obj/MeshLoader.o 

obj/Model.o: src/Model.cpp
	g++ -c -Iinclude src/Model.cpp -std=c++14 -o obj/Model.o

obj/Mesh.o: src/Mesh.cpp
	g++ -c src/Mesh.cpp -std=c++14 -o obj/Mesh.o

obj/OffscreenRender.o: src/OffscreenRender.cpp
	g++ -c -Iinclude src/OffscreenRender.cpp -std=c++14 -o obj/OffscreenRender.o

obj/ShaderPipeline.o: src/ShaderPipeline.cpp
	g++ -c src/ShaderPipeline.cpp -std=c++14 -o obj/ShaderPipeline.o

obj/VertexShader.o: src/VertexShader.cpp
	g++ -c src/VertexShader.cpp -std=c++14 -o obj/VertexShader.o

obj/FragmentShader.o: src/FragmentShader.cpp
	g++ -c src/FragmentShader.cpp -std=c++14 -o obj/FragmentShader.o

obj/Shader.o: src/Shader.cpp
	g++ -c src/Shader.cpp -std=c++14 -o obj/Shader.o

clean:
	rm obj/*
	rm out/*

run:
	./out/main

.PHONY: clean run