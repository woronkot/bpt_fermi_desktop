#pragma once

#ifdef WINDOWS
	#include <GL/glew.h>
	#include <GL/wglew.h>
#endif
#include <GL/freeglut.h>

#include <wx/wx.h>
#include <wx/glcanvas.h>

#include "App.h"
#include "Controls.h"

#define TIMER_ID 1

class Canvas : public wxGLCanvas {
	public:
		Canvas(wxFrame* parent, int* args);
		~Canvas();
		void mouseMoved(wxMouseEvent& event);
		void leftDown(wxMouseEvent& event);
		void leftUp(wxMouseEvent& event);
		void rightDown(wxMouseEvent& event);
		void rightUp(wxMouseEvent& event);
		void mouseWheelMoved(wxMouseEvent& event);
		void resize(wxSizeEvent& evt);
		void Paintit(wxPaintEvent& event);
		void onTimer(wxTimerEvent& event);
	protected:
		DECLARE_EVENT_TABLE()
	private:
		void Render();
	private:
		wxGLContext*	m_context;
		bool			initialized;
		wxTimer			m_timer;
};

