#pragma once

#include <iostream>
#include <vector>

#ifdef WINDOWS
	#include <GL/glew.h>
#endif
#include <GL/freeglut.h>

#include "Shader.h"
#include "VertexShader.h"
#include "FragmentShader.h"

class ShaderPipeline {
    public:
        ShaderPipeline(const char* vertFile, const char* fragFile);
        ShaderPipeline(const VertexShader& vertex, const FragmentShader& frag);
        ~ShaderPipeline();   
        GLuint getProgramID() const;
    private:
        void addShader(const Shader& shader);
        void attach();
    private:
        std::vector<GLuint> shaders;
        GLuint program;
};