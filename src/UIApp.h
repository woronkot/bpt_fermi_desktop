#pragma once
#ifdef WINDOWS
	#include <GL/glew.h>
#endif
#include <wx/wx.h>

#include "UI.h"
#include "Canvas.h"


class UIApp : public wxApp {
	public:
		bool OnInit();
		int OnExit();
};