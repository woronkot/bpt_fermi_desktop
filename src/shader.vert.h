#pragma once

#include <string>

std::string shader_vert = 
"attribute vec3 normal;"
"attribute vec3 color;"
""
"varying float intensity;"
""
"void main() {"
"	vec3 lightDir = normalize(vec3(gl_ProjectionMatrix*gl_LightSource[0].position));"
"	intensity = dot(lightDir,normal);"
"	gl_FrontColor = vec4(color,1.0);"
"	gl_Position = gl_ModelViewProjectionMatrix * vec4(gl_Vertex.xyz, 1);"
"}";