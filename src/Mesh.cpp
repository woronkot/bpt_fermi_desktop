#include "Mesh.h"

Mesh::Mesh() {
    vao = 0;
    vbo[VERTEX_BUFFER] = 0;
    vbo[TEXCOORD_BUFFER] = 0;
    vbo[NORMAL_BUFFER] = 0;
    vbo[INDEX_BUFFER] = 0;
    vbo[COLOR_BUFFER] = 0;

    #ifdef WINDOWS
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
    #else
        glGenVertexArraysAPPLE(1, &vao);
        glBindVertexArrayAPPLE(vao);
    #endif
}

Mesh::~Mesh() {
    if(vbo[VERTEX_BUFFER]) {
		glDeleteBuffers(1, &vbo[VERTEX_BUFFER]);
	}

	if(vbo[TEXCOORD_BUFFER]) {
		glDeleteBuffers(1, &vbo[TEXCOORD_BUFFER]);
	}

	if(vbo[NORMAL_BUFFER]) {
		glDeleteBuffers(1, &vbo[NORMAL_BUFFER]);
	}

	if(vbo[INDEX_BUFFER]) {
		glDeleteBuffers(1, &vbo[INDEX_BUFFER]);
    }

    if(vbo[COLOR_BUFFER]) {
		glDeleteBuffers(1, &vbo[COLOR_BUFFER]);
    }

    #ifdef WINDOWS
        glDeleteVertexArrays(1, &vao);
    #else
        glDeleteVertexArraysAPPLE(1, &vao);
    #endif
}