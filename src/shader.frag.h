#pragma once

#include <string>

std::string shader_frag =
"varying float intensity;"
""
"void main() {"
"    gl_FragColor = (intensity*gl_Color + vec4(0.1,0.1,0.1,0.0));"
"}";

