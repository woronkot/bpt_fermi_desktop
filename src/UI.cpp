#include "UI.h"
#include "App.h"
#include "desktop.xpm"

BEGIN_EVENT_TABLE(UI, wxFrame)
EVT_CLOSE(UI::OnClose)
EVT_MENU(OpenID, UI::onOpen)
EVT_MENU(ExitID, UI::onExit)
EVT_MENU(InfoID, UI::onInfo)
EVT_MENU(AboutID, UI::onAbout)
END_EVENT_TABLE()

UI::UI(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
	: wxFrame(parent, id, title, position, size, style) {
	CreateGUIControls();
}

UI::~UI() {
}

void UI::CreateGUIControls() {

	notSupportedMessage = new wxMessageDialog(this, _("File format not supported!"), _("Error"), wxOK | wxICON_ERROR);

	fileDialog = new wxFileDialog(this, _("Choose a file"), _(""), _(""), _("*.*"), wxFD_OPEN);

	infoMessage = new wxMessageDialog(this, _("Model viewer made using the following libraries:\n-OpenGL\n-Assimp\n-wxWidgets\nFeatures:\n-Support for over 20 file formats\n-Dynamic resolution scaling\n-FXAA antialising"), _("Info"));

	aboutMessage = new wxMessageDialog(this, _("3D Model Viewer\n by Tezeusz Woronko"), _("About"));

	menu = new wxMenuBar();
	wxMenu *ID_MNU_FILE_1001_Mnu_Obj = new wxMenu();
	ID_MNU_FILE_1001_Mnu_Obj->Append(OpenID, _("Open"), _(""), wxITEM_NORMAL);
	ID_MNU_FILE_1001_Mnu_Obj->AppendSeparator();
	ID_MNU_FILE_1001_Mnu_Obj->Append(ExitID, _("Exit"), _(""), wxITEM_NORMAL);
	menu->Append(ID_MNU_FILE_1001_Mnu_Obj, _("File"));

	wxMenu *ID_MNU_HELP_1007_Mnu_Obj = new wxMenu();
	ID_MNU_HELP_1007_Mnu_Obj->Append(InfoID, _("Info"), _(""), wxITEM_NORMAL);
	ID_MNU_HELP_1007_Mnu_Obj->AppendSeparator();
	ID_MNU_HELP_1007_Mnu_Obj->Append(AboutID, _("About"), _(""), wxITEM_NORMAL);
	menu->Append(ID_MNU_HELP_1007_Mnu_Obj, _("Help"));
	SetMenuBar(menu);

	SetTitle(_("3D Model Viewer"));
	SetIcon(_643c4c13eab4f0ae2b17ea9e328e316);
	SetSize(0, 0, 640, 480);
	Center();
}

void UI::OnClose(wxCloseEvent& event) {
	Destroy();
	exit(0);
}

void UI::onExit(wxCommandEvent& event) {
	exit(0);
}

void UI::onOpen(wxCommandEvent& event) {
	if (fileDialog->ShowModal() == wxID_CANCEL) {
		return;
	}
	App::Clear();
	App::Load(fileDialog->GetPath().ToStdString().c_str());

}

void UI::onInfo(wxCommandEvent& event) {
	infoMessage->ShowModal();
}


void UI::onAbout(wxCommandEvent& event) {
	aboutMessage->ShowModal();
}
