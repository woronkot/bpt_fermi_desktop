#pragma once


#ifdef WINDOWS
	#include <GL/glew.h>
#endif
#include <GL/freeglut.h>
#include <vector>

#include "Shader.h"

class FragmentShader : public Shader {
    public:
        FragmentShader(const std::string& source);
        ~FragmentShader();
};