#pragma once

#include <iostream>
#include <ctime>
#include <ratio>
#include <chrono>
#include <cmath>
#ifdef WINDOWS
	#include <GL/glew.h>
#endif
#include <GL/freeglut.h>

#include "OffscreenRender.h"
#include "ShaderPipeline.h"
#include "VertexShader.h"
#include "FragmentShader.h"
#include "MeshLoader.h"
#include "Controls.h"

#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480

#define FRAME_TIME_EPS 3.0
#define CUT_RESOLUTION_RATIO (4.0/5.0)
#define MIN_RESOLUTION_RATIO 0.1
#define MAX_RESOLUTION_RATIO 1.0
#define FRAME_TIME 16.0

#define BG_COLOR (253.0/255.0)

class App {
    public:
        static void Init(int* argc, char ** argv);
        static void Load(const char* path);
		static void Clear();
        static void Render();
		static void HandleFPS();
		static void Resize(int width, int height);

        ~App();
    public:
        static unsigned GetWindowWidth();
        static unsigned GetWindowHeight();
    private:
        App(int* argc, char** argv);
        App(const App&) = delete;        
        static App& getInstance(int* argc = nullptr, char ** argv = nullptr);
        void initOpenGL();  
        void initCallbacks();
        static void display();
        static void display(int value);
        static void reshape(int width, int height); 
        void operator=(const App&) = delete;
    private:
        double lastFrameTime;
        unsigned windowWidth;
        unsigned windowHeight;   
    private:
        MeshLoader loader;
        ShaderPipeline* pipeline;
        OffscreenRender* deferred;
};