#include "OffscreenRender.h"
#include "App.h"
#include "pass.vert.h"
#include "pass.frag.h"


OffscreenRender::OffscreenRender() : resolutionFactor(1), frameBuffer(0), textureBuffer(0), depthBuffer(0), drawBuffers(GL_COLOR_ATTACHMENT0), pipeline(nullptr), status(false), activated(false) {
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "Deferred rendering not supported!" << std::endl;
	pipeline = new ShaderPipeline(VertexShader(pass_vert), FragmentShader(pass_frag));

}

OffscreenRender::~OffscreenRender() {
    if(activated) {
        clear();
        if(pipeline) {
            delete pipeline;
            pipeline = nullptr;
        }
    }
}

void OffscreenRender::toggle(bool status) {
    this->status = status;
    init();
}

void OffscreenRender::attach() {
    if(activated && status) {
        glBindFramebuffer(GL_FRAMEBUFFER, this->frameBuffer);
        glViewport(0,0,(GLsizei)(App::GetWindowWidth()*this->resolutionFactor), (GLsizei)(App::GetWindowHeight()*this->resolutionFactor));
    }
}

void OffscreenRender::detach() {
    if(activated && status) {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0,0,App::GetWindowWidth(), App::GetWindowHeight());
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(this->pipeline->getProgramID());

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, this->textureBuffer);
        glUniform1i(this->texID,0);
        glUniform1f(this->wwID,(GLfloat)(App::GetWindowWidth()));
        glUniform1f(this->whID, (GLfloat)App::GetWindowHeight());
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, this->quadVBO);
        glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE, 0, (void*)0);

        glDrawArrays(GL_TRIANGLES, 0, 6);

        glDisableVertexAttribArray(0);
        glUseProgram(0);
    }
    
}

void OffscreenRender::init() {
    if(!activated) {
        activated = true;
        glGenFramebuffers(1, &this->frameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, this->frameBuffer);

        glGenTextures(1, &this->textureBuffer);
        glBindTexture(GL_TEXTURE_2D, this->textureBuffer);

        glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, (GLsizei)(App::GetWindowWidth()*this->resolutionFactor), (GLsizei)(App::GetWindowHeight()*this->resolutionFactor), 0,GL_RGB, GL_UNSIGNED_BYTE, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

        glGenRenderbuffers(1, &this->depthBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, this->depthBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, (GLsizei)(App::GetWindowWidth()*this->resolutionFactor), (GLsizei)(App::GetWindowHeight()*this->resolutionFactor));
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, this->depthBuffer);

        glFramebufferTextureEXT(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, this->textureBuffer, 0);
        glDrawBuffers(1, &this->drawBuffers); 

        GLfloat g_quad_vertex_buffer_data[] = { 
            -1.0f, -1.0f, 0.0f,
            1.0f, -1.0f, 0.0f,
           -1.0f,  1.0f, 0.0f,
           -1.0f,  1.0f, 0.0f,
            1.0f, -1.0f, 0.0f,
            1.0f,  1.0f, 0.0f,
        };

        glGenBuffers(1, &this->quadVBO);
        glBindBuffer(GL_ARRAY_BUFFER, this->quadVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(g_quad_vertex_buffer_data), g_quad_vertex_buffer_data, GL_STATIC_DRAW);
    
        texID = glGetUniformLocation(this->pipeline->getProgramID(), "textureBuffer");
        wwID = glGetUniformLocation(this->pipeline->getProgramID(), "ww");
        whID = glGetUniformLocation(this->pipeline->getProgramID(), "wh");
    }
}

void OffscreenRender::clear() {
	activated = false;
	glDeleteBuffers(1, &this->quadVBO);
	glDeleteRenderbuffers(1, &this->depthBuffer);
	glDeleteBuffers(1, &this->drawBuffers);
	glDeleteFramebuffers(1, &this->frameBuffer);

	glDeleteTextures(1, &this->textureBuffer);



}

double OffscreenRender::getResolutionFactor() const {
    return this->resolutionFactor;
}

void OffscreenRender::setResolutionFactor(double resFactor) {
    if(this->resolutionFactor != resFactor) {
        this->resolutionFactor = resFactor;
        clear();
        init();
    }

}