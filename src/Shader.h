#pragma once

#include <iostream>
#include <fstream>
#include <string>

#ifdef WINDOWS
	#include <GL/glew.h>
#endif
#include <GL/freeglut.h>
#include <streambuf>

class Shader {
    public:
        Shader(const char* filename);
        Shader(const std::string& source);
        ~Shader();
        GLuint getShaderID() const;
    protected:
        GLuint shaderID;
        std::string shaderSource;
};