#include "VertexShader.h"

VertexShader::VertexShader(const std::string& sourceData) : Shader(sourceData) {
	this->shaderID = glCreateShader(GL_VERTEX_SHADER);
    const GLchar *source = (const GLchar *)this->shaderSource.c_str();
    glShaderSource(shaderID, 1, &source, 0);
    glCompileShader(shaderID);
    GLint isCompiled = 0;
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &isCompiled);
    if(isCompiled == GL_FALSE) {
        GLint maxLength = 0;
        glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &maxLength);
        std::vector<GLchar> infoLog(maxLength);
        glGetShaderInfoLog(shaderID, maxLength, &maxLength, &infoLog[0]);
        std::cout << infoLog.data() << std::endl;        
        glDeleteShader(shaderID);
    }

}

VertexShader::~VertexShader() {

}

