#pragma once

#ifdef WINDOWS
	#include <GL/glew.h>
#endif
#include <wx/wx.h>
#include <wx/frame.h>

#include <wx/filedlg.h>
#include <wx/msgdlg.h>
#include <wx/progdlg.h>
#include <wx/menu.h>

#undef UI_STYLE
#define UI_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX

class UI : public wxFrame {
private:
	DECLARE_EVENT_TABLE();

	public:
		UI(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("3D Model Viewer"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = UI_STYLE);
		virtual ~UI();
		void onExit(wxCommandEvent& event);
		void onOpen(wxCommandEvent& event);
		void onInfo(wxCommandEvent& event);
		void onAbout(wxCommandEvent& event);

	private:
		wxMessageDialog *notSupportedMessage;
		wxFileDialog *fileDialog;
		wxMessageDialog *infoMessage;
		wxMessageDialog *aboutMessage;
		wxMenuBar *menu;

	private:
		enum
		{
			FileID = 1001,
			OpenID = 1002,
			ExitID = 1006,
			HelpID = 1007,
			InfoID = 1008,
			AboutID = 1009
		};

	private:
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();
};

