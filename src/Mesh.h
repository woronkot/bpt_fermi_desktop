#pragma once

#ifdef WINDOWS
	#include <GL/glew.h>
#endif
#include <GL/freeglut.h>

#define MAX_BUFFER 255

class Mesh {
    public:
        Mesh();
        virtual ~Mesh() = 0;
        virtual void draw() = 0;
    protected:
        //NVIDIA BUFFER IDS
        enum BUFFERS {
            VERTEX_BUFFER = 0, 
            NORMAL_BUFFER = 1, 
            COLOR_BUFFER = 2,
            TEXCOORD_BUFFER = 3,
            INDEX_BUFFER = 4
        };
        unsigned elementCount;
		GLuint vao;
		GLuint vbo[MAX_BUFFER];
};