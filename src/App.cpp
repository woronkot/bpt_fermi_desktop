#include "App.h"
#include "shader.vert.h"
#include "shader.frag.h"

App::App(int* argc, char** argv) : windowWidth(WINDOW_WIDTH), windowHeight(WINDOW_HEIGHT), pipeline(nullptr), deferred(nullptr) {
}

App::~App() {
    if(pipeline)
        delete pipeline;
    if(deferred)
        delete deferred;
}

void App::Init(int* argc, char ** argv) {
    App& app = App::getInstance(argc, argv);
    app.initCallbacks();
    app.initOpenGL();
    app.pipeline = new ShaderPipeline(VertexShader(shader_vert),FragmentShader(shader_frag));
    app.deferred = new OffscreenRender();
    app.deferred->toggle(true);
	app.deferred->setResolutionFactor(0.5);
	app.deferred->setResolutionFactor(1);
    Controls::Add();
}

void App::Load(const char* path) {
    App& app = App::getInstance();
    app.loader.loadMeshes(path);
}

void App::Clear() {
	App& app = App::getInstance();
	app.loader.clearMeshes();
	Controls::Reset();
}

void App::Render() {
	App::display();
}

unsigned App::GetWindowWidth() {
    App& app = App::getInstance();
    return app.windowWidth;
}

unsigned App::GetWindowHeight() {
    App& app = App::getInstance();
    return app.windowHeight;
}

App& App::getInstance(int* argc, char ** argv) {
    static App instance(argc, argv);
    return instance;
}

void App::initOpenGL() {
    reshape(this->windowWidth, this->windowWidth);
    glClearColor((GLclampf)BG_COLOR, (GLclampf)BG_COLOR, (GLclampf)BG_COLOR, (GLclampf)1.0);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_SMOOTH);
    glEnable(GL_NORMALIZE);
    glEnable(GL_TEXTURE);
    GLfloat light_diffuse[] = {.6f, .6f, .6f, 1.0f};  
    GLfloat light_position[] = {1.0f, 1.0f, 1.0f, 0.0f}; 
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);

    #ifdef DEBUG
		glEnable(GL_DEBUG_OUTPUT);
    #endif
}

void App::initCallbacks() {
    //glutTimerFunc(1, App::display, 1);
    //glutDisplayFunc(App::display);
    //glutReshapeFunc(App::reshape);
}

void App::display() {
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    App& app = App::getInstance();    
    
    app.deferred->attach();    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);       
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
	Controls::HandleControls();

    glUseProgram(app.pipeline->getProgramID());
    app.loader.drawMeshes();
    glUseProgram(0);
    app.deferred->detach();

    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    app.lastFrameTime = std::chrono::duration<double, std::milli>(t2-t1).count();
    
	std::cout << "Frame time: " << app.lastFrameTime;
	std::cout << " resolution ratio: " << app.deferred->getResolutionFactor() << std::endl;
	//glutPostRedisplay();
}

void App::display(int /*value*/) {
    App::display();
}

void App::reshape(int width, int height) {
    App& app = App::getInstance();
    app.windowWidth = width;
    app.windowHeight = height;
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0,0, width, height);
    gluPerspective(45.0f, (double)app.windowWidth / (double)app.windowHeight, 0.1f, 10000.0f);
}

void App::HandleFPS() {
	App& app = App::getInstance();
	if (app.lastFrameTime > FRAME_TIME + FRAME_TIME_EPS) {
		app.deferred->setResolutionFactor(app.deferred->getResolutionFactor() * CUT_RESOLUTION_RATIO);
		if (app.deferred->getResolutionFactor() < MIN_RESOLUTION_RATIO) {
			app.deferred->setResolutionFactor(MIN_RESOLUTION_RATIO);
		}
		App::reshape(App::GetWindowWidth(), App::GetWindowHeight());
	}
	if (app.lastFrameTime < FRAME_TIME - FRAME_TIME_EPS) {
		app.deferred->setResolutionFactor(app.deferred->getResolutionFactor() / CUT_RESOLUTION_RATIO);
		if (app.deferred->getResolutionFactor() > MAX_RESOLUTION_RATIO) {
			app.deferred->setResolutionFactor(MAX_RESOLUTION_RATIO);
		}
		App::reshape(App::GetWindowWidth(), App::GetWindowHeight());
	}
}

void App::Resize(int width, int height) {
	App& app = App::getInstance();
	app.reshape(width, height);
}
