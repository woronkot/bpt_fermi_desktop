#include "Controls.h"

Controls::Controls() : zoomSensitivity(ZOOM_SENSITIVITY), panSensitivity(PAN_SENSITIVITY), orbitSensitivity(ORBIT_SENSITIVITY),
    pan(false), zoom(1.0f),
    panDX(0.0f), panDY(0.0f),
    orbitDX(0.0f), orbitDY((float)-M_PI/2.0f) {
}

void Controls::Add() {
    Controls& controls = Controls::getInstance();
}

void Controls::Reset() {
	Controls& controls = Controls::getInstance();
	controls.zoom = 1.0f;
	controls.panDX = 0.0f;
	controls.panDY = 0.0f;
	controls.orbitDX = 0.0f;
	controls.orbitDY = (float)-M_PI / 2.0f;
}

Controls& Controls::getInstance() {
    static Controls instance;
    return instance;
}

void Controls::onMouseClick(int button, int state, int x, int y) {
    Controls& controls = Controls::getInstance();    
    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
        controls.orbit = true;                        
        controls.mouseX = x;
        controls.mouseY = y;
    }
    else if( button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
        controls.orbit = false;
    }
    if(button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
        controls.pan = true;                        
        controls.mouseX = x;
        controls.mouseY = y;
    }
    else if( button == GLUT_RIGHT_BUTTON && state == GLUT_UP) {
        controls.pan = false;
    }
    else if(button == GLUT_SCROLL_WHEEL_UP) {
        controls.zoom /= controls.zoomSensitivity;
    }
    else if(button == GLUT_SCROLL_WHEEL_DOWN) {
        controls.zoom *= controls.zoomSensitivity;
    }
}

void Controls::onMouseMove(int x, int y) {
    Controls& controls = Controls::getInstance();
    if(controls.pan) {
        controls.panDX += (float)(controls.mouseX - x) / (App::GetWindowWidth() * controls.panSensitivity); 
        controls.panDY += (float)(controls.mouseY - y) / (App::GetWindowHeight() * controls.panSensitivity); 
    }
    if(controls.orbit) {
        controls.orbitDX -= (float)(controls.mouseX - x) / (App::GetWindowWidth() * controls.orbitSensitivity); 
        controls.orbitDY -= (float)(controls.mouseY - y) / (App::GetWindowHeight() * controls.orbitSensitivity); 
        if(controls.orbitDY > 0) {
            controls.orbitDY = 0;
        }
        if(controls.orbitDY < -M_PI) {
            controls.orbitDY = -(float)M_PI;
        }
    }
    controls.mouseX = x;
    controls.mouseY = y;
}

void Controls::HandleControls() {
    
    Controls& controls = Controls::getInstance();
        
    float x0 = controls.zoom * RADIUS * cos(controls.orbitDX) * sin(controls.orbitDY);
    float y0 = controls.zoom * RADIUS * sin(controls.orbitDX) * sin(controls.orbitDY);
    float z0 = controls.zoom * RADIUS * cos(controls.orbitDY);

    float x1 = controls.zoom * RADIUS * cos(controls.orbitDX) * sin(controls.orbitDY + DELTA);
    float y1 = controls.zoom * RADIUS * sin(controls.orbitDX) * sin(controls.orbitDY + DELTA);
    float z1 = controls.zoom * RADIUS * cos(controls.orbitDY + DELTA);

    glTranslatef(-controls.panDX, controls.panDY, 0);
    gluLookAt(x0,z0,y0, 0,0,0, x1,z1,y0);
}