#include "Canvas.h"
#include <wx/setup.h>

BEGIN_EVENT_TABLE(Canvas, wxGLCanvas)
EVT_MOTION(Canvas::mouseMoved)
EVT_LEFT_DOWN(Canvas::leftDown)
EVT_LEFT_UP(Canvas::leftUp)
EVT_RIGHT_DOWN(Canvas::rightDown)
EVT_RIGHT_UP(Canvas::rightUp)
EVT_SIZE(Canvas::resize)
EVT_MOUSEWHEEL(Canvas::mouseWheelMoved)
EVT_PAINT(Canvas::Paintit)
EVT_TIMER(TIMER_ID, Canvas::onTimer)
END_EVENT_TABLE()

Canvas::Canvas(wxFrame *parent, int * args)
	: wxGLCanvas(parent, wxID_ANY, args, wxDefaultPosition, wxDefaultSize, wxFULL_REPAINT_ON_RESIZE),
	 m_timer(this, TIMER_ID)
{
	m_timer.Start(FRAME_TIME);
	initialized = false;
	m_context = new wxGLContext(this);
	SetBackgroundStyle(wxBG_STYLE_CUSTOM);
}

Canvas::~Canvas() {
	delete m_context;
}


void Canvas::Paintit(wxPaintEvent& WXUNUSED(event)) {	
	if (!initialized) {
		initialized = true;
		int argc = 1;
		char* argv[1] = { wxString((wxTheApp->argv)[0]).char_str() };
		SetCurrent(*m_context);

		glutInit(&argc, argv);

#ifdef WINDOWS
		glewExperimental = TRUE;
		GLenum err = glewInit();
		if (err != GLEW_OK) {
			std::cout << "ERROR initializing GLEW" << std::endl;
		}
	
#endif // WINDOWS

		App::Init(&argc, argv);
		App::Resize(GetSize().x, GetSize().y);
	}

	Render();
}

void Canvas::onTimer(wxTimerEvent & event) {
	Refresh();
}

void Canvas::Render() {
	if (!IsShown()) return;
	wxGLCanvas::SetCurrent(*m_context);
	wxPaintDC(this);
	App::Render();
	glFlush();
	SwapBuffers();
	App::HandleFPS();
}

void Canvas::mouseMoved(wxMouseEvent& event) {
	Controls::onMouseMove(event.GetPosition().x, event.GetPosition().y);
	Refresh();
}

void Canvas::leftDown(wxMouseEvent& event) {
	Controls::onMouseClick(GLUT_LEFT_BUTTON, GLUT_DOWN, event.GetPosition().x, event.GetPosition().y);
	Refresh();
}

void Canvas::mouseWheelMoved(wxMouseEvent& event) {
	if (event.GetWheelRotation() > 0) {
		Controls::onMouseClick(GLUT_SCROLL_WHEEL_UP, 0, event.GetPosition().x, event.GetPosition().y);
	}
	if (event.GetWheelRotation() < 0) {
		Controls::onMouseClick(GLUT_SCROLL_WHEEL_DOWN, 0, event.GetPosition().x, event.GetPosition().y);
	}
	Refresh();
}

void Canvas::leftUp(wxMouseEvent& event) {
	Controls::onMouseClick(GLUT_LEFT_BUTTON, GLUT_UP, event.GetPosition().x, event.GetPosition().y);
	Refresh();
}

void Canvas::rightDown(wxMouseEvent& event) {
	Controls::onMouseClick(GLUT_RIGHT_BUTTON, GLUT_DOWN, event.GetPosition().x, event.GetPosition().y);
	Refresh();
}

void Canvas::rightUp(wxMouseEvent& event) {
	Controls::onMouseClick(GLUT_RIGHT_BUTTON, GLUT_UP, event.GetPosition().x, event.GetPosition().y);
	Refresh();
}


void Canvas::resize(wxSizeEvent& evt) {
	App::Resize(GetSize().x, GetSize().y);
	Refresh();
}