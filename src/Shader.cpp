#include "Shader.h"

Shader::Shader(const char* filename): shaderID(0) {
    std::ifstream ifs(filename);
    
    ifs.seekg(0, std::ios::end);   
    shaderSource.reserve((size_t)ifs.tellg());
    ifs.seekg(0, std::ios::beg);
    
    shaderSource.assign((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
}

Shader::Shader(const std::string& source) {
    shaderSource.assign(source.begin(), source.end());
}

Shader::~Shader() {
    shaderSource.clear();
}

GLuint Shader::getShaderID() const {
    return this->shaderID;
}