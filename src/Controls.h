#pragma once
#define _USE_MATH_DEFINES
#include <cmath>

#ifdef WINDOWS
	#include <GL/glew.h>
#endif
#include <GL/freeglut.h>

#include "App.h"

#define ZOOM_SENSITIVITY 1.01f
#define PAN_SENSITIVITY 0.01f
#define ORBIT_SENSITIVITY 1.0f

#define GLUT_SCROLL_WHEEL_UP 3
#define GLUT_SCROLL_WHEEL_DOWN 4

#define RAD2DEG 57.2957795 
#define DEG2RAD 0.0174532925

#define DELTA 1e-1f

#define RADIUS 100.0f


class Controls {
    public:
        static void Add();
		static void Reset();
        static void HandleControls();     
		static void onMouseClick(int button, int state, int x, int y);
		static void onMouseMove(int x, int y);
    private:
        Controls();
        Controls(const Controls&) = delete;        
        static Controls& getInstance();
        void operator=(const Controls&) = delete;
    private:
        float zoomSensitivity;
        float panSensitivity;
        float orbitSensitivity;
    private:
        int mouseX, mouseY;
        bool pan;        
        float panDX, panDY;
        bool orbit;
        float orbitDX, orbitDY;
        float zoom;
};