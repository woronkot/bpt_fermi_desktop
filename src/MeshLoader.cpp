#include "MeshLoader.h"

MeshLoader::MeshLoader() {

}

void MeshLoader::loadMeshes(const char* path) {
    Assimp::Importer importer; 
	wxProgressDialog* progress = new wxProgressDialog("Loading", "Loading 3D model");
	const aiScene* scene = importer.ReadFile( path ,aiProcessPreset_TargetRealtime_Fast);
    if(!scene) {
		wxMessageBox("Invalid file", "Error");
    }
    else {
		progress->SetRange(scene->mNumMeshes);
		for(unsigned i = 0 ; i < scene->mNumMeshes; i++) {
			progress->Update(i);
            aiMesh* mesh = scene->mMeshes[i];
            aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
            std::unique_ptr<Mesh> model(new Model(mesh, material));
            this->meshes.push_back(std::move(model));
        }
    }
	delete progress;

    return;
}

void MeshLoader::drawMeshes() {
    for(auto&& mesh : this->meshes) {
        mesh->draw();
    }
}

void MeshLoader::clearMeshes() {
	this->meshes.clear();
}
