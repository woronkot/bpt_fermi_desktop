#include "ShaderPipeline.h"

ShaderPipeline::ShaderPipeline(const char* vertFile, const char* fragFile) : program(0){
    VertexShader vert(vertFile);
    this->addShader(vert);
    FragmentShader frag(fragFile);
    this->addShader(frag);
    this->attach();
}

ShaderPipeline::ShaderPipeline(const VertexShader& vertex, const FragmentShader& frag) : program(0){
    this->addShader(vertex);
    this->addShader(frag);
    this->attach();
}

ShaderPipeline::~ShaderPipeline() {

}

GLuint ShaderPipeline::getProgramID() const {
    return this->program;
}

void ShaderPipeline::addShader(const Shader& shader) {
    this->shaders.push_back(shader.getShaderID());
}

void ShaderPipeline::attach() {
    this->program = glCreateProgram();

    for(auto shader : this->shaders) {
        glAttachShader(program, shader);        
    }

    glLinkProgram(program);

    GLint isLinked = 0;
    glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);
    if(isLinked == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);
        std::vector<GLchar> infoLog(maxLength);
        glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);
        glDeleteProgram(program);
        return;
    }

    for(auto shader : this->shaders) {
        glDetachShader(program, shader);        
    }
}