#pragma once

#ifdef WINDOWS
	#include <GL/glew.h>
#endif
#include <GL/freeglut.h>

#include "VertexShader.h"
#include "FragmentShader.h"
#include "ShaderPipeline.h"

class OffscreenRender {
    public:
        OffscreenRender();
        ~OffscreenRender();
        void toggle(bool status);
        void attach();
        void detach();
    public:
        double getResolutionFactor() const;
        void setResolutionFactor(double resFactor);
    private:
        void init();
        void clear();
    private:
        double resolutionFactor;
    private:
        ShaderPipeline* pipeline;
    private:
        GLuint frameBuffer;
        GLuint textureBuffer;
        GLuint depthBuffer;
        GLenum drawBuffers;
        GLuint quadVBO;
        GLuint texID;
        GLuint wwID;
        GLuint whID;
    private:
        bool status;
        bool activated;
};
