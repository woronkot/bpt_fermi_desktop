#pragma once

#include <vector>
#include <iostream>
#ifdef WINDOWS
	#include <GL/glew.h>
#endif
#include <GL/freeglut.h>

#include "Shader.h"

class VertexShader : public Shader {
    public:
        VertexShader(const std::string& source);
        ~VertexShader();
};