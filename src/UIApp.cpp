#include "UIApp.h"

bool UIApp::OnInit()
{
	UI* frame = new UI(NULL);
	int args[] = { WX_GL_RGBA, WX_GL_DOUBLEBUFFER, 0 };
	new Canvas(frame, args);
	SetTopWindow(frame);
	frame->Show();
	return true;
}

int UIApp::OnExit()
{
	return 0;
}
