#pragma once

#include <iostream>
#include <memory>
#include <vector>
#ifdef WINDOWS
	#include <GL/glew.h>
#endif
#include <GL/freeglut.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <wx/progdlg.h>
#include <wx/wx.h>

#include "Model.h"
#include "Mesh.h"


class MeshLoader {
    public:
        MeshLoader();
        void loadMeshes(const char* path);
        void drawMeshes();
		void clearMeshes();
    private:
        std::vector<std::unique_ptr<Mesh> > meshes;
};