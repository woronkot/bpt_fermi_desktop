#pragma once

#include <vector>

#include <assimp/mesh.h>
#include <assimp/material.h>

#include "Mesh.h"


class Model : public Mesh {
    public:
        Model(aiMesh* mesh = nullptr, aiMaterial* material = nullptr);
        ~Model();
        void draw() override;
};