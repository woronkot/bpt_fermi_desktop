#include "Model.h"
#include <iostream>


Model::Model(aiMesh* mesh, aiMaterial* material) : Mesh() {
    if(mesh != nullptr) {
        elementCount = mesh->mNumFaces * 3;
        if(mesh->HasPositions()) {
            float *vertices = new float[mesh->mNumVertices * 3];
            for(unsigned i = 0; i < mesh->mNumVertices; ++i) {
                vertices[i * 3] = mesh->mVertices[i].x;
                vertices[i * 3 + 1] = mesh->mVertices[i].y;
                vertices[i * 3 + 2] = mesh->mVertices[i].z;
            }
    
            glGenBuffers(1, &vbo[VERTEX_BUFFER]);
            glBindBuffer(GL_ARRAY_BUFFER, vbo[VERTEX_BUFFER]);
            glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mNumVertices * sizeof(GLfloat), vertices, GL_STATIC_DRAW);
            glEnableVertexAttribArray (VERTEX_BUFFER);
            glVertexAttribPointer(VERTEX_BUFFER, 3, GL_FLOAT, GL_FALSE, 0, NULL);
            delete[] vertices;
        }

        if(mesh->HasTextureCoords(0)) {
            float *texCoords = new float[mesh->mNumVertices * 2];
            for(unsigned i = 0; i < mesh->mNumVertices; ++i) {
                texCoords[i * 2] = mesh->mTextureCoords[0][i].x;
                texCoords[i * 2 + 1] = mesh->mTextureCoords[0][i].y;
            }
    
            glGenBuffers(1, &vbo[TEXCOORD_BUFFER]);
            glBindBuffer(GL_ARRAY_BUFFER, vbo[TEXCOORD_BUFFER]);
            glBufferData(GL_ARRAY_BUFFER, 2 * mesh->mNumVertices * sizeof(GLfloat), texCoords, GL_STATIC_DRAW);
            glEnableVertexAttribArray (TEXCOORD_BUFFER);
            glVertexAttribPointer(TEXCOORD_BUFFER, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    
            delete[] texCoords;
        }
    
    
        if(mesh->HasNormals()) {
            float *normals = new float[mesh->mNumVertices * 3];
            for(unsigned i = 0; i < mesh->mNumVertices; ++i) {
                normals[i * 3] = mesh->mNormals[i].x;
                normals[i * 3 + 1] = mesh->mNormals[i].y;
                normals[i * 3 + 2] = mesh->mNormals[i].z;
            }
    
            glGenBuffers(1, &vbo[NORMAL_BUFFER]);
            glBindBuffer(GL_ARRAY_BUFFER, vbo[NORMAL_BUFFER]);
            glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mNumVertices * sizeof(GLfloat), normals, GL_STATIC_DRAW);
    
            glEnableVertexAttribArray (NORMAL_BUFFER);
            glVertexAttribPointer(NORMAL_BUFFER, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
            delete[] normals;
        }
        
    
        if(mesh->HasFaces()) {
            unsigned int *indices = new unsigned int[mesh->mNumFaces * 3];
            for(unsigned i = 0; i < mesh->mNumFaces; ++i) {
                indices[i * 3] = mesh->mFaces[i].mIndices[0];
                indices[i * 3 + 1] = mesh->mFaces[i].mIndices[1];
                indices[i * 3 + 2] = mesh->mFaces[i].mIndices[2];
            }
    
            glGenBuffers(1, &vbo[INDEX_BUFFER]);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[INDEX_BUFFER]);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * mesh->mNumFaces * sizeof(GLuint), indices, GL_STATIC_DRAW);
    
            glEnableVertexAttribArray (INDEX_BUFFER);
            glVertexAttribPointer(INDEX_BUFFER, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
            delete[] indices;
        }

        if(mesh->HasPositions() && material) {
            float *vertexColors = new float[mesh->mNumVertices * 3];
            aiColor3D assimpColor (0.0f,0.0f,0.0f);
            material->Get(AI_MATKEY_COLOR_DIFFUSE, assimpColor);
            for(unsigned i = 0; i < mesh->mNumVertices; ++i) {
                vertexColors[i * 3] = assimpColor.r;
                vertexColors[i * 3 + 1] = assimpColor.g;
                vertexColors[i * 3 + 2] = assimpColor.b;
            }
    
            glGenBuffers(1, &vbo[COLOR_BUFFER]);
            glBindBuffer(GL_ARRAY_BUFFER, vbo[COLOR_BUFFER]);
            glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mNumVertices * sizeof(GLfloat), vertexColors, GL_STATIC_DRAW);
            
            glEnableVertexAttribArray (COLOR_BUFFER);
            glVertexAttribPointer(COLOR_BUFFER, 3, GL_FLOAT, GL_FALSE, 0, NULL);
            
            delete[] vertexColors;
        }

    
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        #ifdef WINDOWS
            glBindVertexArray(0);
        #else
            glBindVertexArrayAPPLE(0);
        #endif


    }

}

Model::~Model() {
    
}


void Model::draw() {
    #ifdef WINDOWS
        glBindVertexArray(vao);
    #else
        glBindVertexArrayAPPLE(vao);
    #endif
    
    glDrawElements(GL_TRIANGLES, elementCount, GL_UNSIGNED_INT, NULL);
    
	#ifdef WINDOWS
        glBindVertexArray(0);
    #else
        glBindVertexArrayAPPLE(0);
    #endif
}