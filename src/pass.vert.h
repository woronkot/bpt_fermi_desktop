#pragma once

#include <string>

std::string pass_vert = 
"varying vec2 UV;"
""
"void main(){"
"	gl_Position =  vec4(gl_Vertex.xyz,1);"
"	UV = (gl_Vertex.xy+vec2(1,1))/2.0;"
"}";
